package com.example.demo.sevImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.modal.Student;
import com.example.demo.service.StudentService;
import com.example.demo.studentRep.StudentRepo;
import com.example.demo.studentRepImp.StudentRepoImp;

@Service
public class ServiceImpl implements StudentService {
	
	@Autowired
	StudentRepo studentRepoImp; 

	public ServiceImpl()  {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void saveStudent(Student stu) {
		
		studentRepoImp.save(stu);
		
		
		
	}
	
	public void delStudents() {
		studentRepoImp.deleteAll();
	}
	

}
