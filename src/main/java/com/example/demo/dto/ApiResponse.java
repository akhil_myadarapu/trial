package com.example.demo.dto;

public class ApiResponse {
	
	public ApiResponse(String s) {
		message=s;
		
	}
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 

}
