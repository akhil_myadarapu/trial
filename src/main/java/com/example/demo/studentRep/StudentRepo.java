package com.example.demo.studentRep;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.modal.Student;


public interface StudentRepo extends JpaRepository<Student,Integer> {
	
	public Student save(Student stu);
	public void deleteAllInBatch();
	//public Student findByName();

}
