package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.modal.Student;
import com.example.demo.service.StudentService;
import com.example.demo.dto.ApiResponse;

@RestController
@RequestMapping("/college")
public class StudentContoller {
	
	@Autowired
	StudentService student;

	public StudentContoller() {
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping("/student")
	public ResponseEntity<ApiResponse> message(@RequestBody Student student ) {
		this.student.saveStudent(student);
		
		//System.out.println("Student");
		return ResponseEntity.ok(new ApiResponse("Student verified successfully"));
	}
	
	@PostMapping("/delete")
	public ResponseEntity<ApiResponse> deleteAll( ) {
		this.student.delStudents();
		
		//System.out.println("Student");
		return ResponseEntity.ok(new ApiResponse("Students deleted"));
	}

}
